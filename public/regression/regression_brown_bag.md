<style>
.center {
display: block;
margin-left: auto;
margin-right: auto;
width: 50%;
}
.small-code pre code {
font-size: 1em;
}

blockquote {
margin-bottom: 3em;
}

</style>
Regression Brown Bag
========================================================
author: Jaleal Sanjak
date: September 2018
autosize: true

Regression
========================================================

Regression is methods for studying the relationship between a **response variable Y** and a **covariate X**

- It is the most popular method for studying the relationships between variables
- The main goal is to estimate the **regression function** which describes the **expected value** of Y **conditional on** X 

***

![alt text](regression_brown_bag-figure/Linear_regression.svg "Logo Title Text 1")

The Simple Linear Model
========================================================
$$
Y_i = \beta_0 + \beta_1 * X_i + \epsilon_i
$$

The Simple Linear Model
========================================================
$$
\underbrace{Y_i}_{\substack{\text{Response for} \\ \text{individual i}}} =\beta_0 + \beta_1 * \underbrace{X_i}_{\substack{\text{Covariate for} \\ \text{individual i}}} +\underbrace{\epsilon_i}_{\substack{\text{Error for} \\ \text{individual i}}}
$$

The Simple Linear Model
========================================================
$$
\underbrace{Y_i}_{\substack{\text{Response for} \\ \text{individual i}}} = \overbrace{\beta_0}^{\text{Intercept}} + \overbrace{\beta_1}^{\text{Slope}} * \underbrace{X_i}_{\substack{\text{Covariate for} \\ \text{individual i}}} +\underbrace{\epsilon_i}_{\substack{\text{Error for} \\ \text{individual i}}}
$$

The Normal Linear Model
========================================================
$$
\underbrace{Y_i}_{\substack{\text{Response for} \\ \text{individual i}}} = \overbrace{\beta_0}^{\text{Intercept}} + \overbrace{\beta_1}^{\text{Slope}} * \underbrace{X_i}_{\substack{\text{Covariate for} \\ \text{individual i}}} +\underbrace{\epsilon_i}_{\substack{\text{Error for} \\ \text{individual i}}}
$$

$$
\underbrace{\epsilon \sim \mathcal{N}(0,\sigma^{2}_{e})}_{\text{Normal random variable}}
$$
- For the purposes of statistical modelling we assume that errors are normally distributed with constant variance 
- Can be extened to other error models
- Not required when you don't care about statistical properties 
(e.g. you only care about prediction)


Visualizing the model
========================================================

<img src="regression_brown_bag-figure/example_regression.png"  alt="drawing" style="width:800px"/>


Visualizing the model
========================================================

<img src="regression_brown_bag-figure/example_regression_labeled.png"  alt="drawing" style="width:800px"/>


Visualizing the model
========================================================

<img src="regression_brown_bag-figure/example_regression_labeled.png"  alt="drawing" style="width:800px"/>

Line Fitting: Ordinary Least Squares
========================================================

- Minimize the **sum of squared errors**
- This is solved analytically with **OLS estimator** 
- $Y = X\beta + \epsilon$
- $\hat{\beta} = (X^TX)^{-1}X^TY$
- If we assume that errors (residuals) are normally distributed with mean zero, then this is also the  **Maximum Likelihood Estimate** 

***
<img src="regression_brown_bag-figure/example_regression_labeled.png"  alt="drawing" />


Interpreting the fit: mean estimates
========================================================
Fitted values are the **regression line** for the **observed X values**
$$\hat{Y} = \hat{\beta_0} + \hat{\beta}*X$$

Residuals are the **predicted errors**:
$$\hat{\epsilon} =  Y - \hat{Y}$$

Interpreting the fit: variance estimates
========================================================
If we assume that errors (residuals) are normally distributed with mean zero, then an **unbiased estimate** of the **error variance** is:
$$\hat{\sigma_{e}}^2 =\underbrace{ \frac{1}{n-p}}_{\substack{\text{# of observations} \\ \text{minus # of covariates}}} * \sum_{i=1}^{n} \hat{\epsilon}^2$$

We consider the regression coefficients to be **random variables** with standard errors
$$\hat{se}(\hat{\beta_1}) = \frac{\hat{\sigma_{e}}}{\hat{\sigma_X}\sqrt{n}}$$

Line Fitting: Recap
========================================================
We solved for the **least squares solution** and obtained estimates of:
- Regression coefficients: $\hat{\beta}$
- The standard errors of the regression coefficient estimates: $\hat{se}(\hat{\beta})$
- Fitted values: $\hat{Y}$
- Error variance: $\hat{\sigma_e}^2$

***
<img src="regression_brown_bag-figure/example_regression_labeled_fit.png"  alt="drawing" />


Frequentist Questions
========================================================

What can a frequentist analysis give us:
- Test's against the **null hypothesis** that $\beta = 0$
- Construct **confidence intervals** that trap the slope $\beta$ 95% of the time
- Construct **confidence intervals** that trap the predicted value $\hat{Y}$ 95% of time
- Construct **prediction intervals** that trap **individual new observations** 95% of the time


Example: Beak Size in Darwin's Finches
========================================================

<img src="regression_brown_bag-figure/unnamed-chunk-1-1.png" title="plot of chunk unnamed-chunk-1" alt="plot of chunk unnamed-chunk-1" style="display: block; margin: auto;" />

Example: Beak Size in Darwin's Finches
========================================================


```r
finch_model <- lm(offspring_beak_depth ~ 1 + parent_beak_depth, data=finch_beak_data)
```

Example: Beak Size in Darwin's Finches
========================================================
class: small-code

```

Call:
lm(formula = offspring_beak_depth ~ 1 + parent_beak_depth, data = finch_beak_data)

Residuals:
     Min       1Q   Median       3Q      Max 
-1.61882 -0.27625  0.04603  0.26900  1.46325 

Coefficients:
                  Estimate Std. Error t value Pr(>|t|)    
(Intercept)        2.44902    0.31904   7.676 1.21e-13 ***
parent_beak_depth  0.72277    0.03355  21.545  < 2e-16 ***
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Residual standard error: 0.4705 on 411 degrees of freedom
Multiple R-squared:  0.5304,	Adjusted R-squared:  0.5292 
F-statistic: 464.2 on 1 and 411 DF,  p-value: < 2.2e-16
```

Regression line confidence intervals
========================================================
The frequentist supposes that the regression coefficients $\hat{\beta}$ are **random variables** 

$$\underbrace{\hat{\beta} \sim \mathcal{N}(\beta,\hat{se}(\hat{\beta}))}_{\substack{
\text{The estimated regression coefficient} \\ 
\text{is a random variable drawn from } \\ 
\text{distribution centered at the true} \\
\text{population parameter, with an unknown}\\
\text{variance that we also estimate from our data}}}$$

And that the predicted value (regression line) $\hat{Y}$ is a **composite random variable** constructed from the regression coefficients.

$$\hat{Y} = \hat{\beta_0} + \hat{\beta_1} * X$$


Regression line confidence intervals
========================================================

<img src="regression_brown_bag-figure/unnamed-chunk-4-1.png" title="plot of chunk unnamed-chunk-4" alt="plot of chunk unnamed-chunk-4" style="display: block; margin: auto;" />


Regression line prediction intervals
========================================================
Recall that the predicted value (regression line) $\hat{Y}$ is a **composite random variable** constructed from the regression coefficients.

$$\hat{Y} = \hat{\beta_0} + \hat{\beta_1} * X$$

But, a **new observed data point** will be the **predicted value plus error**

$$Y^* = \hat{Y^*} + \epsilon^*$$

Therefore it's variance includes the **error variance** $\hat{\sigma_e}^2$

Regression line prediction intervals
========================================================
<img src="regression_brown_bag-figure/unnamed-chunk-5-1.png" title="plot of chunk unnamed-chunk-5" alt="plot of chunk unnamed-chunk-5" style="display: block; margin: auto;" />


What do these intervals mean?
========================================================

"It will be noticed that in the above description, the probability statements refer to the problems of estimation with which the statistician will be concered in the futer. In face, I have repeatedly stated that the frequency of correct results will tend to $\alpha$. Consider now the case when a sample is already drawn, and the calculations have given [particular limits]. Can we say that in the this particular case the probability of the true value [falling between this limits] is equal to $\alpha$? The answer is obviously in the negative. The parameter is an unknown constant, and no probability statement concerning its value may be made..." 

Flipping the Script: Bayesian Perspectives
========================================================
The bayesian derivation starts with a probabilistic statement about the **response variable**

$$\underbrace{Y|\beta, \sigma^{2}_{e},X}_{\substack{
\text{The response variable conditional} \\
\text{on the regression coefficient, } \\
\text{the error variance and the } \\
\text{covariate data}
}} \sim \underbrace{\mathcal{N}(X\beta,\sigma^{2}_{e}I)}_{
\substack{\text{A normal distribution}\\
\text{with mean at the regression line}\\
\text{and variance equal to the error variance}}
}$$

Flipping the Script: Bayesian Perspectives
========================================================
The bayesian derivation starts with a probabilistic statement about the **response variable**

$$\underbrace{Y|\beta, \sigma^{2}_{e},X}_{\substack{
\text{The response variable conditional} \\
\text{on the regression coefficient, } \\
\text{the constant variance and the } \\
\text{covariate data}
}} \sim \underbrace{\mathcal{N}(X\beta,\sigma^{2}_I)}_{
\substack{\text{A normal distribution}\\
\text{with mean at the regression line}\\
\text{and a constant variance }}
}$$

Flipping the Script: Bayesian Perspectives
========================================================
Now, we invoke **Bayes Rule**!

$$p(\beta,\sigma^2|Y)=\frac{\overbrace{p(\beta,\sigma^2)}^{\substack{\text{Joint prior on} \\ \text{the parameters}}} \overbrace{p(Y|\beta,\sigma^2)}^{\substack{
\text{The likelihood of the response}\\
\text{given the parameters}
}}}{\underbrace{p(Y)}_{
\substack{\text{The marginal distribution} \\
\text{of the response}}}}$$

Defining priors
========================================================

- A major benefit of Bayesian methods is the ability to use **prior information**
- There are many choices for the prior in a linear model
- Today I'll use a **non-informative** prior to draw analogies to the frequentist analysis


Non-informative priors
========================================================
- The non-informative prior here is uniform on the regression coefficients and uniform on a log scale for the variance:
$$p(\beta,\sigma) = p(\beta |\sigma)*p(\sigma)$$

$$p(\beta|\sigma) \propto 1$$
$$p(\sigma) \propto \frac{1}{\sigma^2}$$

Note, these are **improper priors**, which means they are **NOT REAL PROBABILITY DISTRIBUTIONS** and serve as mathematical tools to our hard work easier

Back to Darwins finches
========================================================
class:small-code
- The posterior distribution of the parameter ends up with some familiar terms

$$p(\beta|\sigma^2,Y) \sim \mathcal{N}(\hat{\beta}_{OLS},\hat{se}(\hat{\beta}_{OLS}))$$

- In many Bayesian applications, we use Monte Carlo simulation to generate the distributions of our estimates. Here are the results from that type of analysis:

```r
finch_model <- lm(offspring_beak_depth ~ 1 + parent_beak_depth, data=finch_beak_data)
bf<-bayesfit(finch_model,10000)
res <- t(apply(bf,2,Bayes.sum))
print(res) 
```

```
                       mean         se        t  CrI.2.5% CrI.97.5%
(Intercept)       2.4547174 0.31982831  7.67511 1.8332212 3.0830042
parent_beak_depth 0.7222250 0.03362591 21.47823 0.6563896 0.7880990
sigma             0.4715081 0.01637723 28.79047 0.4406577 0.5044531
```


Back to Darwins finches
========================================================
- It is often easier to visualize the results in the form of histograms
- The black horizontal bars the OLS estimates (same as frequentist)
<img src="regression_brown_bag-figure/unnamed-chunk-7-1.png" title="plot of chunk unnamed-chunk-7" alt="plot of chunk unnamed-chunk-7" style="display: block; margin: auto;" />

Bayesian credible intervals: Regression line
========================================================

Using our samples (or analytical results) we can determine the range of values that contain 95% of all predicted values. This is similar to the frequentist confidence interval for the regression

Bayesian credible intervals: Regression line
========================================================

<img src="regression_brown_bag-figure/unnamed-chunk-8-1.png" title="plot of chunk unnamed-chunk-8" alt="plot of chunk unnamed-chunk-8" style="display: block; margin: auto;" />

Bayesian credible intervals: Posterior predictive distribution
========================================================

Using our samples (or analytical results) we can determine the range of values that we predict to  contain 95% of all future observed values. This is similar to the frequentist prediction interval


Bayesian credible intervals: Posterior predictive distribution
========================================================

<img src="regression_brown_bag-figure/unnamed-chunk-9-1.png" title="plot of chunk unnamed-chunk-9" alt="plot of chunk unnamed-chunk-9" style="display: block; margin: auto;" />

Bayesian vs Frequentist
========================================================

Frequentist version

<img src="regression_brown_bag-figure/unnamed-chunk-10-1.png" title="plot of chunk unnamed-chunk-10" alt="plot of chunk unnamed-chunk-10" style="display: block; margin: auto;" />
***
Bayesian version

<img src="regression_brown_bag-figure/unnamed-chunk-11-1.png" title="plot of chunk unnamed-chunk-11" alt="plot of chunk unnamed-chunk-11" style="display: block; margin: auto;" />

Gimme my money back!
========================================================

<img src="regression_brown_bag-figure/outragecat.jpg"  alt="drawing" style="width:800px"/>

This equivalence is not true in general!
========================================================
- The equivalence is simultaneously shallow and deep
  - The intervals are approximately same, but mean completely different things
  - The equivalence connects the two perspectives
- The use of the non-informative prior is what made these two be essentially equivalent
- There are many other priors:
  - Gaussian priors (like ridge regression)
  - Laplace priors (like the LASSO regression)
  - Spike and Slab priors
  - Priors based on SME's or previous experiments
  
Pluses and Minuses
========================================================

Frequentist:
- Easier mathematically and computationally today
- Allows for tests for statistical significance against null hypotheses
- Allows you to construct ranges that have a (1-$\alpha$)% shot at catching the true value

***

Bayesian:
- A bit trickier technically
- Allows you to talk about your relative belief in different possible realities
- Allows you to incorporate other beliefs
- Allows for regularization in a probabilistic framework
  
